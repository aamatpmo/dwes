<?php

namespace AppBundle\BLL;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Alimento;

class AlimentoBLL extends BaseBLL
{
    public function getAlimento($id)
    {
        $alimento = $this->em->getRepository('AppBundle:Alimento')->find($id);

        if($alimento === null)
            throw new NotFoundHttpException();

        return $alimento;
    }

    public function getAlimentos()
    {
        return $this->em->getRepository('AppBundle:Alimento')->findAll();
    }

    public function nuevoAlimento($nombre, $energia, $proteina, $hidratocarbono, $fibra, $grasatotal, $tipo)
    {
        $alimento = new Alimento();
        $alimento->setNombre($nombre);
        $alimento->setEnergia($energia);
        $alimento->setProteina($proteina);
        $alimento->setHidratocarbono($hidratocarbono);
        $alimento->setFibra($fibra);
        $alimento->setGrasatotal($grasatotal);
        $alimento->setTipo($tipo);

        $errors = $this->validator->validate($alimento);

        if (count($errors) > 0)
            return $errors;

        $this->guarda($alimento);

        return array();
    }

    public function editaAlimento(
        $alimento, $nombre, $energia, $proteina, $hidratocarbono, $fibra, $grasatotal, $tipo)
    {
        $alimento->setNombre($nombre);
        $alimento->setEnergia($energia);
        $alimento->setProteina($proteina);
        $alimento->setHidratocarbono($hidratocarbono);
        $alimento->setFibra($fibra);
        $alimento->setGrasatotal($grasatotal);
        $alimento->setTipo($tipo);

        $errors = $this->validator->validate($alimento);

        if (count($errors) > 0)
            return $errors;

        $this->guarda($alimento);

        return array();
    }
}