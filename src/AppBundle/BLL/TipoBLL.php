<?php
namespace AppBundle\BLL;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TipoBLL extends BaseBLL
{
    public function getTipo($id)
    {
        $tipo = $this->em->getRepository('AppBundle:Tipo')->find($id);

        if($tipo === null)
            throw new NotFoundHttpException();

        return $tipo;
    }

}