<?php

namespace AppBundle\Model;

class Config
{
    static public $mvc_bd_hostname = "localhost";
    static public $mvc_bd_nombre = "alimentos";
    static public $mvc_bd_usuario = "alimentos";
    static public $mvc_bd_clave = "alimentos";
    static public $mvc_vis_css = "main.css";
}