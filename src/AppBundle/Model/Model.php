<?php

namespace AppBundle\Model;

class Model
{
    protected $conexion;

    public function __construct($dbname, $dbuser, $dbpass, $dbhost)
    {
        $mvc_bd_conexion = new \mysqli($dbhost, $dbuser, $dbpass);
        $error = $mvc_bd_conexion->connect_errno;
        if ($error != null)
        {
            die('No ha sido posible realizar la conexión con la base de datos:'.
                $mvc_bd_conexion->connect_error);
        }
        $mvc_bd_conexion->select_db($dbname);
        $mvc_bd_conexion->set_charset('utf8');
        $this->conexion = $mvc_bd_conexion;
    }

    private function dameAlimentosDB($sql)
    {
        $result = $this->conexion->query($sql);
        $alimentos = array();
        while ($row = $result->fetch_assoc())
        {
            $alimentos[] = $row;
        }
        return $alimentos;
    }

    public function dameAlimentos()
    {
        $sql = "select * from alimentos order by energia desc";
        return $this->dameAlimentosDB($sql);
    }

    public function buscarAlimentosPorNombre($nombre)
    {
        $nombre = htmlspecialchars($nombre);
        $sql = "select * from alimentos where nombre like '".$nombre.
            "' order by energia desc";
        return $this->dameAlimentosDB($sql);
    }

    public function dameAlimento($id)
    {
        $id = htmlspecialchars($id);
        $sql = "select * from alimentos where id=".$id;
        $alimentos = $this->dameAlimentosDB($sql);
        if (count($alimentos) > 0)
            return $alimentos[0];
        else
            return null;
    }

    public function insertarAlimento($n, $e, $p, $hc, $f, $g)
    {
        $n = htmlspecialchars($n);
        $e = htmlspecialchars($e);
        $p = htmlspecialchars($p);
        $hc = htmlspecialchars($hc);
        $f = htmlspecialchars($f);
        $g = htmlspecialchars($g);
        $sql = "insert into alimentos (nombre, energia, proteina, ".
            "hidratocarbono, fibra, grasatotal) values ('".$n."',".$e.",".
            $p.",".$hc.",".$f.",".$g.")";
        $result = $this->conexion->query($sql);
        return $result;
    }

    public function actualizarAlimento($n, $e, $p, $hc, $f, $g)
    {
        $n = htmlspecialchars($n);
        $e = htmlspecialchars($e);
        $p = htmlspecialchars($p);
        $hc = htmlspecialchars($hc);
        $f = htmlspecialchars($f);
        $g = htmlspecialchars($g);
        $sql = "update alimentos set nombre=$n, energia=%e, proteina=$p, ".
            "hidratocarbono=$hc, fibra=$f, grasatotal=$g)";
        $result = $this->conexion->query($sql);
        return $result;
    }

    public function validarDatos($n, $e, $p, $hc, $f, $g)
    {
        return (is_string($n) &
            is_numeric($e) &
            is_numeric($p) &
            is_numeric($hc) &
            is_numeric($f) &
            is_numeric($g));
    }
}