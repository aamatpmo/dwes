<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Model\Model;
use AppBundle\Model\Config;
use AppBundle\Entity\Alimento;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Security;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="route_homepage")
     * @Template("AppBundle:Default:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        return array(
            'mensaje' => 'Bienvenido al curso de Symfony2',
            'fecha' => date('d-m-Y'),
        );
    }

    /**
     * @Route("/contacto", name="route_contacto")
     * @Template("AppBundle:Default:contacto.html.twig")
     */
    public function contactoAction(Request $request)
    {
        return array(
            'mensaje' => 'Somos alumnos de DWES',
            'fecha' => date('d-m-Y'),
        );
    }

    /**
     * @Route("/login", name="route_login")
     * @Template("AppBundle:Default:login.html.twig")
     */
    public function loginAction(Request $request)
    {
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR))
        {
            $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
        }
        else
        {
            $error = $this->get('session')->get(Security::AUTHENTICATION_ERROR);
        }

        return array(
                'last_username' => $request->getSession()->get(Security::LAST_USERNAME),
                'error' => $error);
    }
}
