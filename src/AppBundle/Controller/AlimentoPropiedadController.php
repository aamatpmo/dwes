<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Model\Model;
use AppBundle\Model\Config;
use AppBundle\Entity\Alimento;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/alimento/{id}/propiedad", requirements={"id" = "\d+"})
 */
class AlimentoPropiedadController extends Controller
{
    /**
     * @Route("/create", name="route_add_propiedad")
     * @Method({"POST"})
     */
    public function createPropiedadAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $idPropiedad = $request->request->get('propiedad');

        $alimento = $em->getRepository('AppBundle:Alimento')->find($id);
        $propiedad = $em->getRepository('AppBundle:Propiedad')->find($idPropiedad);

        $propiedades = $alimento->getPropiedades();

        if ($propiedades->contains($propiedad))
        {
            $this->addFlash(
                'error',
                'La propiedad seleccionada ya existe para el alimento '.$alimento->getNombre()
            );
        }
        else
        {
            $alimento->getPropiedades()->add($propiedad);

            $em->persist($alimento);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('route_edit', array('id' => $id)));
    }

    /**
     * @Route("/{idPropiedad}/delete", name="route_delete_propiedad")
     * @Method({"GET"})
     */
    public function deletePropiedadAction($id, $idPropiedad)
    {
        $em = $this->getDoctrine()->getManager();

        $alimento = $em->getRepository('AppBundle:Alimento')->find($id);
        $propiedad = $em->getRepository('AppBundle:Propiedad')->find($idPropiedad);

        $propiedadEliminada = $alimento->getPropiedades()->removeElement($propiedad);

        if ($propiedadEliminada === true)
        {
            $em->persist($alimento);
            $em->flush();
        }
        else
        {
            $this->addFlash(
                'error',
                'La propiedad seleccionada no existe para el alimento '.$alimento->getNombre()
            );
        }

        return $this->redirect($this->generateUrl('route_edit', array('id' => $id)));
    }
}
