<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Model\Model;
use AppBundle\Model\Config;
use AppBundle\Form\AlimentoType;
use AppBundle\Entity\Alimento;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/alimento")
 */
class AlimentoController extends Controller
{
    /**
     * @Route("/", name="route_listar")
     * @Template("AppBundle:Default:listar.html.twig")
     * @Method({"GET"})
     */
    public function listarAction()
    {
        return $this->get('dwes.BLL.alimento')->listar('AppBundle:Alimento');
    }

    /**
     * @Route("/{id}", name="route_ver", requirements={"id" = "\d+"})
     * @Template("AppBundle:Default:ver.html.twig")
     * @Method({"GET"})
     */
    public function verAction($id=2)
    {
        $alimento = $this->get('dwes.BLL.alimento')->getAlimento($id);

        return array(
            'alimento' => $alimento
        );
    }

    private function getForm(Alimento &$alimento = null)
    {
        if ($alimento === null)
            $alimento = new Alimento();

        return $this->createForm(new AlimentoType(), $alimento);
    }

    private function getDataInsertar($form)
    {
        $em = $this->getDoctrine()->getManager();

        $tipos = $em->getRepository('AppBundle:Tipo')->findAll();

        return array(
            'form' => $form->createView(),
            'tipos' => $tipos);
    }

    /**
     * @Route("/new", name="route_new")
     * @Template("AppBundle:Default:insertar.html.twig")
     * @Method({"GET"})
     */
    public function newAction()
    {
        $form = $this->getForm();

        return $this->getDataInsertar($form);
    }

    private function actualizaAlimento(Request $request, $alimento = null)
    {
        if ($alimento !== null)
            $edit = true;
        else
            $edit = false;

            $form = $this->getForm($alimento);

        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($alimento);
            $em->flush();

            return $this->redirect($this->generateUrl('route_listar'));
        }

        if ($edit === true)
            return $this->getDataEdit($form, $alimento);
        else
            return $this->getDataInsertar($form);
    }

    /**
     * @Route("/create", name="route_create")
     * @Template("AppBundle:Default:insertar.html.twig")
     * @Method({"POST"})
     */
    public function createAction(Request $request)
    {
        return $this->actualizaAlimento($request);
    }

    private function getDataEdit($form, $alimento)
    {
        $em = $this->getDoctrine()->getManager();

        $tipos = $em->getRepository('AppBundle:Tipo')->findAll();

        $propiedades = $em->getRepository('AppBundle:Propiedad')->findAll();

        $propiedadesAlimento = $alimento->getPropiedades();

        $propiedadesRestantes = array_diff($propiedades, $propiedadesAlimento->toArray());

        return array(
            'alimento' => $alimento,
            'form' => $form->createView(),
            'tipos' => $tipos,
            'propiedadesRestantes' => $propiedadesRestantes);

    }

    /**
     * @Route("/{id}/edit", name="route_edit")
     * @Template("AppBundle:Default:insertar.html.twig")
     * @Method({"GET"})
     */
    public function editAction(Request $request, $id)
    {
        $alimento = $this->get('dwes.BLL.alimento')->getAlimento($id);

        $form = $this->getForm($alimento);

        return $this->getDataEdit($form, $alimento);
    }

    /**
     * @Route("/{id}/update", name="route_update")
     * @Template("AppBundle:Default:insertar.html.twig")
     * @Method({"POST"})
     */
    public function updateAction(Request $request, $id)
    {
        $alimento = $this->get('dwes.BLL.alimento')->getAlimento($id);

        return $this->actualizaAlimento($request, $alimento);
    }

    /**
     * @Route("/{id}/delete", name="route_delete")
     * @Method({"GET"})
     */
    public function deleteAction($id)
    {
        $alimento = $this->get('dwes.BLL.alimento')->getAlimento($id);

        $em = $this->getDoctrine()->getManager();

        $em->remove($alimento);
        $em->flush();

        return $this->redirect($this->generateUrl('route_listar'));
    }

    /**
     * @Route("/buscar", name="route_buscar")
     * @Template("AppBundle:Default:buscar.html.twig")
     * @Method({"GET"})
     */
    public function buscarAction()
    {
        return array();
    }

    /**
     * @Route("/filtrar", name="route_filtrar")
     * @Template("AppBundle:Default:listar.html.twig")
     * @Method({"POST"})
     */
    public function filtrarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $nombre = $request->request->get('nombre');
        $tipo = $request->request->get('tipo');

        if ((!isset($nombre) || $nombre === '')
            && (!isset($tipo) || $tipo === ''))
        {
            $this->addFlash(
                'error',
                'El nombre y el tipo del alimento no pueden quedar vacíos!'
            );

            return $this->redirect($this->generateUrl('route_buscar'));
        }

        $alimentos = $em->getRepository('AppBundle:Alimento')
            ->filtraPorNombreYTipo($nombre, $tipo);

        return array('alimentos' => $alimentos);
    }

    /**
     * @Route("/tipo", name="route_listar_por_tipo")
     * @Template("AppBundle:Default:listarPorTipo.html.twig")
     * @Method({"GET"})
     */
    public function listarPorTipoAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tipos = $em->getRepository('AppBundle:Tipo')->findAll();

        return array(
            'tipos' => $tipos,
        );
    }
}
