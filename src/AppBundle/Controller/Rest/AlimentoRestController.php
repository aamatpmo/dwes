<?php

namespace AppBundle\Controller\Rest;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use AppBundle\Entity\Alimento;
use Symfony\Component\HttpFoundation\Request;

/**
 * @RouteResource("Alimento")
 */
class AlimentoRestController extends Controller implements ClassResourceInterface
{
    public function cgetAction()
    {
        return $this->get('dwes.BLL.alimento')->getAlimentos();
    }

    public function getAction(Alimento $alimento)
    {
        return $alimento;
    }

    public function postAction()
    {
        $request = $this->get('request');

        $nombre = $request->request->get('nombre');
        $energia = $request->request->get('energia');
        $proteina = $request->request->get('proteina');
        $hidratocarbono = $request->request->get('hidratocarbono');
        $fibra = $request->request->get('fibra');
        $grasatotal = $request->request->get('grasatotal');
        $idTipo = $request->request->get('tipo');

        if (!isset($idTipo) || $idTipo === '')
            return array('code' => 500, 'message' => 'Es obligatorio pasar un tipo');

        $tipo = $this->get('dwes.BLL.tipo')->getTipo($idTipo);

        $errores = $this->get('dwes.BLL.alimento')->nuevoAlimento(
            $nombre, $energia, $proteina, $hidratocarbono, $fibra, $grasatotal, $tipo);

        if (count($errores) > 0)
            return $errores;

        return array('code' => 200, 'message' => 'Alimento añadido correctamente');
    }

    public function putAction(Request $request, Alimento $alimento)
    {
        return array('code' => 200, 'message' => 'Alimento editado correctamente');
    }
}