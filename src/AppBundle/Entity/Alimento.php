<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Alimento
 *
 * @ORM\Table(name="alimento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AlimentoRepository")
 * @UniqueEntity("nombre", message="Ya existe un alimento con ese nombre")
 */
class Alimento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Propiedad")
     */
    private $propiedades;

    /**
     * @ORM\ManyToOne(targetEntity="Tipo", inversedBy="alimentos")
     * @ORM\JoinColumn(name="tipo_id", referencedColumnName="id", nullable=false)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="El campo nombre no puede quedarse vacío")
     * @Assert\Length(
     *      max = 10,
     *      maxMessage = "El nombre sólo puede tener {{ limit }} letras")
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="energia", type="integer", options={"default":0})
     * @Assert\NotBlank(message="El campo energía no puede quedarse vacío")
     * @Assert\Range(
     *      min = "0",
     *      max = "100",
     *      minMessage = "La energía debe ser mayor o igual que {{ limit }}",
     *      maxMessage = "La energía debe ser menor o igual que {{ limit }}",
     *      invalidMessage = "La energía debe ser un número"
     * )
     */
    private $energia;

    /**
     * @var int
     *
     * @ORM\Column(name="proteina", type="integer", options={"default":0})
     * @Assert\NotBlank(message="El campo proteina no puede quedarse vacío")
     * @Assert\Range(
     *      min = "0",
     *      max = "100",
     *      minMessage = "La energía debe ser mayor o igual que {{ limit }}",
     *      maxMessage = "La energía debe ser menor o igual que {{ limit }}",
     *      invalidMessage = "La energía debe ser un número"
     * )
     */
    private $proteina;

    /**
     * @var int
     *
     * @ORM\Column(name="hidratocarbono", type="integer", options={"default":0})
     * @Assert\NotBlank(message="El campo hidrato de carbono no puede quedarse vacío")
     * @Assert\Range(
     *      min = "0",
     *      max = "100",
     *      minMessage = "Los hidratos deben ser mayor o igual que {{ limit }}",
     *      maxMessage = "Los hidratos deben ser menor o igual que {{ limit }}",
     *      invalidMessage = "Los hidratos deben ser un número"
     * )
     */
    private $hidratocarbono;

    /**
     * @var int
     *
     * @ORM\Column(name="fibra", type="integer", options={"default":0})
     * @Assert\NotBlank(message="El campo fibra no puede quedarse vacío")
     * @Assert\Range(
     *      min = "0",
     *      max = "100",
     *      minMessage = "La fibra debe ser mayor o igual que {{ limit }}",
     *      maxMessage = "La fibra debe ser menor o igual que {{ limit }}",
     *      invalidMessage = "La fibra debe ser un número"
     * )
     */
    private $fibra;

    /**
     * @var int
     *
     * @ORM\Column(name="grasatotal", type="integer", options={"default":0})
     * @Assert\NotBlank(message="El campo grasa no puede quedarse vacío")
     * @Assert\Range(
     *      min = "0",
     *      max = "100",
     *      minMessage = "La grasa debe ser mayor o igual que {{ limit }}",
     *      maxMessage = "La grasa debe ser menor o igual que {{ limit }}",
     *      invalidMessage = "La grasa debe ser un número"
     * )
     */
    private $grasatotal;

    public function __construct()
    {
        $this->propiedades = new ArrayCollection();
        $this->energia = 0;
        $this->proteina = 0;
        $this->hidratocarbono = 0;
        $this->fibra = 0;
        $this->grasatotal = 0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setEnergia($energia)
    {
        $this->energia = $energia;

        return $this;
    }

    public function getEnergia()
    {
        return $this->energia;
    }

    public function setProteina($proteina)
    {
        $this->proteina = $proteina;

        return $this;
    }

    public function getProteina()
    {
        return $this->proteina;
    }

    public function setHidratocarbono($hidratocarbono)
    {
        $this->hidratocarbono = $hidratocarbono;

        return $this;
    }

    public function getHidratocarbono()
    {
        return $this->hidratocarbono;
    }

    public function setFibra($fibra)
    {
        $this->fibra = $fibra;

        return $this;
    }

    public function getFibra()
    {
        return $this->fibra;
    }

    public function setGrasatotal($grasatotal)
    {
        $this->grasatotal = $grasatotal;

        return $this;
    }

    public function getGrasatotal()
    {
        return $this->grasatotal;
    }

    public function setTipo(Tipo $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getTipo()
    {
        return $this->tipo;
    }

    public function getPropiedades()
    {
        return $this->propiedades;
    }
}
