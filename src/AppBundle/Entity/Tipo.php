<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tipo
 *
 * @ORM\Table(name="tipo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TipoRepository")
 */
class Tipo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Alimento", mappedBy="tipo")
     * @Assert\NotBlank(message="El campo nombre no puede quedarse vacío")
     */
    private $alimentos;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, unique=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    public function __toString()
    {
        return $this->getNombre();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function __construct()
    {
        $this->alimentos = new ArrayCollection();
    }

    public function addAlimento(Alimento $alimento)
    {
        $this->alimentos[] = $alimento;

        return $this;
    }

    public function removeAlimento(Alimento $alimento)
    {
        $this->alimentos->removeElement($alimento);
    }

    public function getAlimentos()
    {
        return $this->alimentos;
    }
}
