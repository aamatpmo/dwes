<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AlimentoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('energia', IntegerType::class)
            ->add('proteina', IntegerType::class)
            ->add('hidratocarbono', IntegerType::class)
            ->add('fibra', IntegerType::class)
            ->add('grasatotal', IntegerType::class)
            ->add('tipo', EntityType::class, array(
                'class' => 'AppBundle:Tipo',
                'choice_label' => 'nombre'));
    }

    public function getName()
    {
        return 'alimento';
    }

    public function configureOptions(OptionsResolver $options)
    {
        $options->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Alimento',
        ));
    }
}
